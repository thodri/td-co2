#include "TriplePointNaCl.h"

using namespace std;

// namespace csmp
// {
  
  TriplePointNaCl::TriplePointNaCl()
    :
    ttriple_nacl(800.7e0),
    ptriple_nacl(5.0e1), // now Pascal
    xtriple_nacl(1.0e0)
  {
  }
  
  TriplePointNaCl::~TriplePointNaCl()
  {
  }
  
// }//csmp
