#include <QCoreApplication>
#include <iostream>
#include <fstream>
#include <cmath>
#include "EOS_CO2H2ONaCl_Spycher2004.h"
#include "HaliteLiquidus.h"
#include "ConvertConcentrationUnitsNaCl.h"
#include "h2o_co2_nacl_enums.h"
#include "phasestatefinder_h2o_co2_nacl.h"


int main()
{

    EOS_CO2H2ONaCl_Spycher04 eos;

    double t = 40.;
    double p = 100.e5;
    double massfraction_h2o{.0001};
    double massfraction_co2{.2};
    double massfraction_nacl{1.-massfraction_h2o-massfraction_co2};
    double total_mass{1.};
    PhaseStateFinder_H2O_CO2_NaCl phasestate(t,p,total_mass,massfraction_h2o,massfraction_co2,massfraction_nacl);
    phasestate.Equilibrate();
    cout << "state     : " << phasestate.State() << endl;
    cout << "mass aq   : " << phasestate.massAqueousPhase() << endl;
    cout << "mass carb : " << phasestate.massCarbonicPhase() << endl;
    cout << "mass salt : " << phasestate.massHalite() << endl;
    cout << "X_h2o     : " << phasestate.X_h2o() << endl;
    cout << "X_co2     : " << phasestate.X_co2() << endl;
    cout << "X_nacl    : " << phasestate.X_nacl() << endl;
    cout << "Y_h2o     : " << phasestate.Y_h2o() << endl;
    cout << "Y_co2     : " << phasestate.Y_co2() << endl;
    cout << "rho aq    : " << phasestate.rho_aq() << endl;
    cout << "rho carb  : " << phasestate.rho_carb() << endl;
    cout << "mu aq     : " << phasestate.mu_aq() << endl;
    cout << "mu carb   : " << phasestate.mu_carb() << endl;
    cout << "beta aq   : " << phasestate.beta_aq() << endl;
    cout << "beta carb : " << phasestate.beta_carb() << endl;
//    int i{1};

//    phasestate.InputComposition();
//    phasestate.Equilibrate(aq_carb_salt);


//    double xb,yb;
//    HaliteLiquidus liquidus(t,p);

//    double xsat = liquidus.MoleFractionNaCl();
//    double msat = XNaCl2Molal(xsat);


     // *** code for fitting ***
//    std::vector<double> t{20., 30., 40., 50., 60., 70., 80., 90., 100.};
//    std::vector<double> p{10.0e5,20.0e5,50.0e5,100.0e5,200.0e5,300.0e5,400.0e5,500.0e5,600.0e5,700.0e5,800.0e5};

//      ofstream ofile("yH2O.dat");
//  //    ofstream ofile("true_xco2.dat");
//    for(const double& myt : t)
//    {
//        for(const double& myp : p)
//        {
//  //            double refvalue = eos.m_Co2(myp,myt,0.)/(55.508+eos.m_Co2(myp,myt,0.));
//            double refvalue = eos.y_H2o(myp,myt,0.);
//            for(double m = 0.; m < 5.; m += 0.5)
//            {
//  //                ofile << m/55.508 << "\t" << eos.y_H2o(myp,myt,m)/(1.-eos.y_H2o(myp,myt,m))/refvalue << endl;
//                double xco2 = eos.m_Co2(myp,myt,m)/(55.508+2.0*m+eos.m_Co2(myp,myt,m));
//                double xh2o = 55.508/(55.508+m+eos.m_Co2(myp,myt,m));
//                double xnacl = 1.0-xco2-xh2o;
//  //                ofile << xnacl << "\t" << xco2/refvalue << endl;
//                ofile << xnacl << "\t" << eos.y_H2o(myp,myt,m)/refvalue << endl;
//            }
//        }
//        ofile << endl;
//    }


    //    // *** for ternary plots ***
    //    ofstream ofile("50C_200bar_ternary.dat");
    //    double t{50.},p{200.e5};
    //    HaliteLiquidus liquidus(t,p);
    //    double MoleFractionNaClAtSaturation{liquidus.MoleFractionNaCl()};
    //    double MolalityNaClAtSaturation{XNaCl2Molal(MoleFractionNaClAtSaturation)};

    //    // cordinates for salt saturation line aqueous+salt
    //    cout << "Endpoints of salt saturation line aqueous :\n";
    //    cout << "0.0\t" << MoleFractionNaClAtSaturation << "\t" << 1.0-MoleFractionNaClAtSaturation << endl;
    //    double xco2 = eos.m_Co2(p,t,MolalityNaClAtSaturation)/(eos.m_Co2(p,t,MolalityNaClAtSaturation)+55.508+MolalityNaClAtSaturation);
    //    cout << xco2 << "\t" << (1.0-xco2)*MoleFractionNaClAtSaturation << "\t" << (1.0-xco2)*(1.0-MoleFractionNaClAtSaturation) << endl;
    //    xco2 = eos.x_Co2(p,t,0.0);
    //    cout << xco2 << "\t" << 1.0-xco2 << "\t0.0\n";



    //    HaliteLiquidus liquidus(myt,myp);

    //    //************** Input from transport code ******************
    //    //
    //    // ArrayVariables for all phases, with the following properties:
    //    //
    //    // mass fractions
    //    //
    //    //************** WANTED RESULTS ******************************
    //    //
    //    // - phase state & saturations
    //    // - densities
    //    // - deltaV
    //    // - phase compositions, mass fractions
    //    // - viscosities etc.
    //    // - Tmax, Pmax a la Spycher
    //    //
    //    //************************************************************


}

