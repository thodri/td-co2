#ifndef H2O_CO2_NACL_ENUMS_H
#define H2O_CO2_NACL_ENUMS_H

    enum state{aq=0,carb=1,salt=2,aq_salt=3,aq_carb=4,carb_salt=5,aq_carb_salt=6,full=7,undefined=8};
    // enum corner uses a convention similar to csmp for the corners of a triangle
    enum corner{a=0,b=1,c=2};

#endif // H2O_CO2_NACL_ENUMS_H
