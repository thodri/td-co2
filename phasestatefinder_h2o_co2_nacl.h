#ifndef PHASESTATEFINDER_H2O_CO2_NACL_H
#define PHASESTATEFINDER_H2O_CO2_NACL_H

#include "h2o_co2_nacl_enums.h"
#include "HaliteLiquidus.h"
#include "EOS_CO2H2ONaCl_Spycher2004.h"

class PhaseStateFinder_H2O_CO2_NaCl
{
    // sorry for all the "publicity"
    // this is a result of the way I developed it, will be cleaned
public:
    PhaseStateFinder_H2O_CO2_NaCl( const double& t, const double& p,
                                   const double& total_mass,
                                   const double& massfraction_h2o, const double& massfraction_co2, const double& massfraction_nacl );

    //  Equilibrate is almost all you need - will find the correct  phase state and compute all fluid properties
    //  that come directly from eos
    void Equilibrate();

    // Access to results, always run Equilibrate before
    double massCarbonicPhase();
    double rho_carb();
    double mu_carb();
    double beta_carb();
    double Y_h2o();
    double Y_co2();

    double massAqueousPhase();
    double rho_aq();
    double mu_aq();
    double beta_aq();
    double X_h2o();
    double X_co2();
    double X_nacl();
    double D_Co2();

    double massHalite();

    state  State();

private:
    PhaseStateFinder_H2O_CO2_NaCl();
    const double& t;                                      ///< temperature [Celsius]
    const double& p;                                      ///< pressure [Pascal]
    const double& total_mass;                             ///< total mass of H2O+CO2+NaCl in fluid phase and NaCl crystalline salt in the control volume [kg]
    const double& bulk_massfraction_h2o;                  ///< fraction of total_mass that is H2O [-]
    const double& bulk_massfraction_co2;                  ///< fraction of total_mass that is CO2 [-]
    const double& bulk_massfraction_nacl;                 ///< fraction of total mass that is NaCl (dissolved and solid) [-]

    const double PI;                                      ///< the number PI
    const double h2o_x;                                   ///< cartesian x-coordinate of the H2O corner of the triangular phase diagram
    const double h2o_y;                                   ///< cartesian y-coordinate of the H2O corner of the triangular phase diagram
    const double co2_x;                                   ///< cartesian x-coordinate of the CO2 corner of the triangular phase diagram
    const double co2_y;                                   ///< cartesian y-coordinate of the CO2 corner of the triangular phase diagram
    const double nacl_x;                                  ///< cartesian x-coordinate of the NaCl corner of the triangular phase diagram
    const double nacl_y;                                  ///< cartesian y-coordinate of the NaCl corner of the triangular phase diagram
    const double molar_mass_h2o;                          ///< [kg mol-1]
    const double molar_mass_co2;                          ///< [kg mol-1]
    const double molar_mass_nacl;                         ///< [kg mol-1]

    double weight_phase_a,weight_phase_b,weight_phase_c;  ///< weights (mole-based) of phases on corner points in triangular regions
    double bulk_xh2o,bulk_xco2,bulk_xnacl;                ///< mole fractions of bulk composition
    double bulk_mnacl;                                    ///< molality of NaCl for the bulk composition
    double x_bulkcomp,y_bulkcomp;                         ///< cartesian coordinates of bulk composition
    double x[3],y[3];                                     ///< corner points of triangular regions within phase diagram
    double xsat;                                          ///< mole fraction of NaCl in salt-saturated but CO2-free water
    double msat;                                          ///< molality of NaCl in salt-saturated aqueous phase (this does not depend on CO2) [mole NaCl / kg H2O]
    double mnacl;                                         ///< general molality variable for NaCl [mole NaCl / kg H2O]

    double massCarbonicPhase_;                            ///< [kg]
    double rho_carb_;                                     ///< Density of carbonic phase [kg m-3]
    double mu_carb_;                                      ///< dynamic viscosity of carbonic phase [Pa s]
    double beta_carb_;                                    ///< isothermal compressibility of carbonic phase [Pa-1]
    double Y_h2o_;                                        ///< mass fraction of H2O in carbonic phase [-]
    double Y_co2_;                                        ///< mass fraction of CO2 in carbonic phase [-]

    double massAqueousPhase_;                             ///< [kg]
    double rho_aq_;                                       ///< Density of aqueous phase [kg m-3]
    double mu_aq_;                                        ///< dynamic viscosity of aqueous phase [Pa s]
    double beta_aq_;                                      ///< isothermal compressibility of aqueous phase [Pa-1]
    double X_h2o_;                                        ///< mass fraction of H2O in aqueous phase [-]
    double X_co2_;                                        ///< mass fraction of CO2 in aqueous phase [-]
    double X_nacl_;                                       ///< mass fraction of NaCl in aqueous phase [-]
    double D_Co2_;                                        ///< Diffusivity of CO2 in the aqueous phase (just copy/pasted from SKM's code, no further info)

    double massHalite_;                                   ///< [kg]
    state mystate;
    HaliteLiquidus liquidus;
    EOS_CO2H2ONaCl_Spycher04 eos;

    // mole fraction composition of phases in triangular or lower dimensional order phase regions, order as "corner" enum
    double corner_xh2o[3];
    double corner_xco2[3];
    double corner_xnacl[3];
    // same thing in mass fraction
    double corner_massfraction_h2o[3];
    double corner_massfraction_co2[3];
    double corner_massfraction_nacl[3];

    void MoleToMassfractionAtTriangularCorners();
    void ConvertMolefractionTriangularCornersToCartesian(corner/*,double xh2o, double xco2, double xnacl*/);
    void ConvertMolefractionToCartesian(double& x, double& y, double xh2o, double xco2, double xnacl);
    bool EvaluateFractionsInTriangularRegion();
    void EvaluateSaltyAqCarb();
    void SetCornerPointsTriangularRegions(state mystate);
};

inline double PhaseStateFinder_H2O_CO2_NaCl::massCarbonicPhase(){ return massCarbonicPhase_; }
inline double PhaseStateFinder_H2O_CO2_NaCl::rho_carb(){ return rho_carb_;}
inline double PhaseStateFinder_H2O_CO2_NaCl::mu_carb(){ return mu_carb_;}
inline double PhaseStateFinder_H2O_CO2_NaCl::beta_carb(){ return beta_carb_;}
inline double PhaseStateFinder_H2O_CO2_NaCl::Y_h2o(){ return Y_h2o_;}
inline double PhaseStateFinder_H2O_CO2_NaCl::Y_co2(){ return Y_co2_;}

inline double PhaseStateFinder_H2O_CO2_NaCl::massAqueousPhase(){ return massAqueousPhase_;}
inline double PhaseStateFinder_H2O_CO2_NaCl::rho_aq(){ return rho_aq_;}
inline double PhaseStateFinder_H2O_CO2_NaCl::mu_aq(){ return mu_aq_;}
inline double PhaseStateFinder_H2O_CO2_NaCl::beta_aq(){ return beta_aq_;}
inline double PhaseStateFinder_H2O_CO2_NaCl::X_h2o(){ return X_h2o_;}
inline double PhaseStateFinder_H2O_CO2_NaCl::X_co2(){ return X_co2_;}
inline double PhaseStateFinder_H2O_CO2_NaCl::X_nacl(){ return X_nacl_;}
inline double PhaseStateFinder_H2O_CO2_NaCl::D_Co2(){ return D_Co2_; }

inline double PhaseStateFinder_H2O_CO2_NaCl::massHalite(){ return massHalite_;}

inline state  PhaseStateFinder_H2O_CO2_NaCl::State(){return mystate;}

#endif // PHASESTATEFINDER_H2O_CO2_NACL_H
